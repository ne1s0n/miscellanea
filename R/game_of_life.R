# This script is a (very quick and dirty) implementation of
# Conway's game of life on a modulus board. No intention of
# building anything serious. It's just a small tribute to
# the man that started the whole thing and that recently
# passed away.

# SUPPORT FUNCTIONS -------------------------------------------------------
#extract the 3x3 neighbourhood around the passed coordinates,
#flattened to a 1x8 array
extract_neighbourhood = function(board, x, y){
  side = nrow(board)
  res = NULL
  for (delta_x in c(+1, 0, side-1)){
  for (delta_y in c(+1, 0, side-1)){
    #skipping the center
    if ((delta_x == 0) && (delta_y == 0)) {next()}
      
    #computing the coordinates of the target, modulus
    x_new = (x - 1 + delta_x) %% side + 1
    y_new = (y - 1 + delta_y) %% side + 1
    
    #updating the neighbourhood
    res = c(res, board[x_new, y_new])
  }}
  
  return(res)
}

#here it's the game of life logic
update_cell = function(current_state, neighbours_alive){
  if (current_state){
    #alive cell, should live or die?
    return(neighbours_alive %in% 2:3)
  }else{
    #empty space, should spawn?
    return(neighbours_alive == 3)
  }
}

#updates the board
step = function(board){
  side = nrow(board)
  new_board = matrix(data = FALSE, nrow = side, ncol = side)
  for (x in 1:side){
  for (y in 1:side){
    neighbourhood = extract_neighbourhood(board, x, y)
    new_board[x, y] = update_cell(board[x,y], sum(neighbourhood))
  }}
  return(new_board)
}

#creates an empty board of the selected size (must be 5+)
empty_board = function(side){
  if (side < 5){
    stop('Side should be 5 or more')
  }
  return(matrix(data = FALSE, nrow = side, ncol = side))
}

#adds a shape at the selected coordinates
add_shape = function(shape, board, x, y){
  side = ncol(board)
  
  for (delta_x in 1:nrow(shape)){
  for (delta_y in 1:ncol(shape)){
    x_board = (x + delta_x - 2) %% side + 1
    y_board = (y + delta_y - 2) %% side + 1
    board[x_board, y_board] = board[x_board, y_board] || shape[delta_x, delta_y]
  }}
  
  return(board)
}

#returns a string representation of the current board
to_string = function(board, put_header = TRUE){
  #⬛ full
  #⬜ empty
  side = ncol(board)
  printable = matrix(data = '⬜', nrow = side, ncol = side)
  printable[board] = '⬛'
  
  #actual returned string
  s = NULL
  if (put_header){
    s = paste(collapse = ' ', rep('#', side))
  }
  for (i in 1:nrow(printable)){
    s = c(s, paste(collapse = ' ', printable[i,]))
  }
  s = paste(collapse='\n', s)
  
  #we add a newline at the end so it's printable with
  #cat without the need to print a newline each time
  s = paste(collapse = '', s, '\n')
  
  return(s)
}

# ACTUAL GAME -------------------------------------------------------------
#standard shapes
glider = matrix(nrow = 3, ncol = 3, byrow = TRUE,
          data = c(FALSE, TRUE,  FALSE,
                  FALSE, FALSE, TRUE,
                  TRUE,  TRUE,  TRUE))
blinker = matrix(nrow = 3, ncol = 3, byrow = TRUE,
            data = c(FALSE, FALSE, FALSE,
                     TRUE,  TRUE,  TRUE,
                     FALSE, FALSE, FALSE))
toad = matrix(nrow = 4, ncol = 4, byrow = TRUE,
         data = c(FALSE, FALSE, FALSE, FALSE,
                  FALSE, TRUE,  TRUE,  TRUE,
                  TRUE,  TRUE,  TRUE, FALSE,
                  FALSE, FALSE, FALSE, FALSE))

#R-pentomino releases gliders, that in our modulus geometry 
#are bound to collide with other stuff. You are advised.
Rpentomino = matrix(nrow = 3, ncol = 3, byrow = TRUE,
              data = c(FALSE, TRUE, TRUE,
                       TRUE,  TRUE, FALSE,
                       FALSE, TRUE, FALSE))

#building the board
current_board = empty_board(15)

#adding a few shapes
current_board = add_shape(glider, current_board, 2, 2)
current_board = add_shape(blinker, current_board, 2, 10)
current_board = add_shape(toad, current_board, 10, 2)

#a nice infinite loop of updates
while(TRUE){
  #this is intended to clear the screen when the script
  #is invoked from command line (e.g. Rscript game_of_life.R)
  #when used inside Rstudio it can ruin the Console output, 
  #so it's better to comment it
  #system('clear')
  
  #printing via cat so '\n' are rendered
  cat(to_string(current_board))
  
  #actually updating the board
  current_board = step(current_board)
  
  #half a second pause to avoid a blinking mess
  Sys.sleep(0.5)
}
